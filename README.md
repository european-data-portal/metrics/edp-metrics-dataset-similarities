# Dataset Similarity Service

Fingerprint datasets for later comparison and provides and endpoint to retrieve similar datasets for a given ID using a distance metric.

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [API](#api)
1. [Docker](#docker)
1. [Configuration](#configuration)
    1. [Data Info Object](#data-info-object)
    1. [Environment](#environment)
    1. [Logging](#logging)


## Build

Requirements:
 * Git
 * Maven 3
 * Java 11

```bash
$ git clone ...
$ mvn package
```

## Run

```bash
$ java -jar target/dataset-similarities.jar
```

## Docker

Build docker image:
```bash
$ docker build -t edp/metrics-dataset-similarities .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 edp/metrics-dataset-similarities
```

## API

A formal OpenAPI 3 specification can be found in the `src/main/resources/webroot/openapi.yaml` file.
A visually more appealing version is available at `{url}:{port}` once the application has been started.

## Configuration

### Environment


| Key | Description | Default |
| :--- | :--- | :--- |
| PORT | Port this service will run on | 8086 |
| API_KEY | Authorization secret required for certain endpoints. Must be configured for service to run. | null |
| WORK_DIR | Directory into which fingerprint files are written | /tmp |
| SPARQL_URL | Address of the SPARQL endpoint | https://www.europeandataportal.eu/sparql |


### Logging
See [logback](https://logback.qos.ch/documentation.html) documentation for more details

| Variable| Description | Default Value |
| :--- | :--- | :--- |
| `PIVEAU_PIPE_LOG_APPENDER` | Configures the log appender for the pipe context | `STDOUT` |
| `PIVEAU_LOGSTASH_HOST`            | The host of the logstash service | `logstash` |
| `PIVEAU_LOGSTASH_PORT`            | The port the logstash service is running | `5044` |
| `PIVEAU_PIPE_LOG_PATH`     | Path to the file for the file appender | `logs/piveau-pipe.%d{yyyy-MM-dd}.log` |
| `PIVEAU_PIPE_LOG_LEVEL`    | The log level for the pipe context | `INFO` |
| `PIVEAU_LOG_LEVEL`    | The general log level for the `io.piveau` package | `INFO` |

