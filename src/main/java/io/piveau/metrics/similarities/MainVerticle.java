package io.piveau.metrics.similarities;

import io.piveau.metrics.similarities.model.SimilarityRequest;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.*;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.RouterFactoryOptions;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainVerticle extends AbstractVerticle {

    private static final Logger LOG = LoggerFactory.getLogger(MainVerticle.class);

    private JsonObject config;
    private ApiKeyHandler apiKeyHandler;

    @Override
    public void start(Promise<Void> startPromise) {
        LOG.info("Launching Dataset Similarity Service...");

        // startup is only successful if no step failed
        Future<Void> steps = loadConfig()
            .compose(handler -> initApiKey())
            .compose(handler -> bootstrapVerticles())
            .compose(handler -> startServer());

        steps.onComplete(handler -> {
            if (handler.succeeded()) {
                LOG.info("Dataset Similarity Service successfully launched");
                startPromise.complete();
            } else {
                LOG.error("Failed to launch Dataset Similarity Service: " + handler.cause());
                startPromise.fail(handler.cause());
            }
        });
    }

    private Future<Void> loadConfig() {
        return Future.future(loadConfig -> {
            ConfigRetriever configRetriever = ConfigRetriever.create(vertx);

            configRetriever.getConfig(handler -> {
                if (handler.succeeded()) {
                    config = handler.result();
                    LOG.debug(config.encodePrettily());

                    configRetriever.listen(change ->
                        config = change.getNewConfiguration());

                    loadConfig.complete();
                } else {
                    loadConfig.fail("Failed to load config: " + handler.cause());
                }
            });
        });
    }

    private Future<Void> initApiKey() {
        return Future.future(initApiKey -> {
            String apiKey = config.getString(ApplicationConfig.ENV_API_KEY);

            if (apiKey != null && !apiKey.isEmpty()) {
                apiKeyHandler = new ApiKeyHandler(apiKey);
                initApiKey.complete();
            } else {
                initApiKey.fail("No API key specified");
            }
        });
    }

    private CompositeFuture bootstrapVerticles() {
        DeploymentOptions options = new DeploymentOptions()
            .setConfig(config)
            .setWorkerPoolName("extractor-pool")
            .setMaxWorkerExecuteTime(30)
            .setMaxWorkerExecuteTimeUnit(TimeUnit.MINUTES)
            .setWorker(true);

        List<Future> deploymentFutures = new ArrayList<>();
        deploymentFutures.add(startVerticle(options, SimilarityVerticle.class.getName()));
        deploymentFutures.add(startVerticle(options, FingerprintVerticle.class.getName()));

        return CompositeFuture.join(deploymentFutures);
    }

    private Future<Void> startServer() {
        return Future.future(startServer -> {
            Integer port = config.getInteger(ApplicationConfig.ENV_APPLICATION_PORT, ApplicationConfig.DEFAULT_APPLICATION_PORT);

            OpenAPI3RouterFactory.create(vertx, "webroot/openapi.yaml", handler -> {
                if (handler.succeeded()) {
                    OpenAPI3RouterFactory routerFactory = handler.result();
                    RouterFactoryOptions options = new RouterFactoryOptions().setMountNotImplementedHandler(true).setMountValidationFailureHandler(true);
                    routerFactory.setOptions(options);

                    routerFactory.addSecurityHandler("ApiKeyAuth", apiKeyHandler::checkApiKey);

                    routerFactory.addHandlerByOperationId("fingerprintLanguages", this::handleFingerprintRequest);
                    routerFactory.addHandlerByOperationId("similaritiesForDataset", this::handleSimilarityRequest);

                    Router router = routerFactory.getRouter();
                    router.route().handler(CorsHandler.create("*").allowedMethod(HttpMethod.GET).allowedHeader("Access-Control-Allow-Origin: *"));
                    router.route("/*").handler(StaticHandler.create());

                    HttpServer server = vertx.createHttpServer(new HttpServerOptions().setPort(port));
                    server.requestHandler(router).listen();

                    LOG.info("Server successfully launched on port [{}]", port);
                    startServer.complete();
                } else {
                    // Something went wrong during router factory initialization
                    LOG.error("Failed to start server at [{}]: {}", port, handler.cause());
                    startServer.fail(handler.cause());
                }
            });
        });
    }

    private void handleFingerprintRequest(RoutingContext context) {
        vertx.eventBus().send(ApplicationConfig.ADDRESS_START_FINGERPRINT, new JsonArray(context.queryParam("language")).encode());
        context.response().setStatusCode(202).end();
    }

    private void handleSimilarityRequest(RoutingContext context) {
        String datasetId = context.pathParam("datasetId");
        List<String> limitList = context.queryParam("limit");

        if (datasetId != null
            && limitList.size() == 1
            && StringUtils.isNumeric(limitList.get(0))) {

            SimilarityRequest request =
                new SimilarityRequest(datasetId, Integer.parseInt(limitList.get(0)));

            vertx.eventBus().send(ApplicationConfig.ADDRESS_GET_SIMILARITY, Json.encode(request), sendHandler -> {
                if (sendHandler.succeeded()) {
                    context.response()
                        .setStatusCode(200)
                        .end((String) sendHandler.result().body());
                } else {
                    context.response().setStatusCode(500).end();
                }
            });
        } else {
            context.response().setStatusCode(400).end();
        }
    }

    private Future<Void> startVerticle(DeploymentOptions options, String className) {
        return Future.future(startVerticle -> {
            vertx.deployVerticle(className, options, handler -> {
                if (handler.succeeded()) {
                    startVerticle.complete();
                } else {
                    LOG.error("Failed to deploy verticle [{}] : {}", className, handler.cause());
                    startVerticle.fail("Failed to deploy [" + className + "] : " + handler.cause());
                }
            });
        });
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }
}
